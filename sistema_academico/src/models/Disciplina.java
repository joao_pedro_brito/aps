/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jpbri
 */
public class Disciplina {
    private String nome;
    private int cod;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNome() {
        return nome;
    }

    public int getCod() {
        return cod;
    }
    
}
