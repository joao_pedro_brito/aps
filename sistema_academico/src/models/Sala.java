/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jpbri
 */
public class Sala {
    private int cod,cod_aluno,cod_sala;

    public void setCod(int cod) {
        this.cod = cod;
    }

    public int getCod_sala() {
        return cod_sala;
    }

    public void setCod_sala(int cod_sala) {
        this.cod_sala = cod_sala;
    }

    public void setCod_aluno(int cod_aluno) {
        this.cod_aluno = cod_aluno;
    }

    public int getCod() {
        return cod;
    }

    public int getCod_aluno() {
        return cod_aluno;
    }
    
}
