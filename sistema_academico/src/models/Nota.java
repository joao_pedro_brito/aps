/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jpbri
 */
public class Nota {
        private int cod,cod_aluno,cod_disciplina;
        private float nota;

    public int getCod() {
        return cod;
    }

    public int getCod_aluno() {
        return cod_aluno;
    }

    public int getCod_disciplina() {
        return cod_disciplina;
    }

    public float getNota() {
        return nota;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public void setCod_aluno(int cod_aluno) {
        this.cod_aluno = cod_aluno;
    }

    public void setCod_disciplina(int cod_disciplina) {
        this.cod_disciplina = cod_disciplina;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }
        
}
