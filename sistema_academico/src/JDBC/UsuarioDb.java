/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import models.Usuario;
/**
 *
 * @author jpbri
 */
public class UsuarioDb {
    private Connection conn;
    private ConectionFactory conexao = new ConectionFactory();
    private ResultSet rs; 
    private PreparedStatement ps; 
    private String listar = "select * from usuario";

    
    public ArrayList<Usuario>findAll(){
     ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
     
     try{
         conn = conexao.conectar();
         ps = conn.prepareStatement(listar);
         rs = ps.executeQuery();
         
         while(rs.next()){
             Usuario u = new Usuario();
             u.setCod(rs.getInt("COD_USUARIO"));
             u.setLogin(rs.getString("LOGIN"));
             u.setSenha(rs.getString("SENHA"));
             
             usuarios.add(u);             
         }
         
     }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
     return usuarios;
    }
    
    public boolean inserir(Usuario u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Insert into usuario (LOGIN,SENHA) values(?,?)");
        ps.setString(1, u.getLogin());
        ps.setString(2, u.getSenha());
        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
   public boolean atualizar(Usuario a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("update usuario set LOGIN = ?,SENHA = ? where COD_USUARIO = ? ");
        ps.setString(1, a.getLogin());
        ps.setString(2, a.getSenha());
        ps.setInt(3, a.getCod());
        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }  
        
         public boolean excluir (Usuario a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Delete from usuario WHERE COD_USUARIO = ?;");
        ps.setInt(1, a.getCod());
        ps.executeUpdate();
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
}
