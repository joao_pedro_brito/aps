/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import models.Disciplina;

/**
 *
 * @author jpbri
 */
public class DisciplinaDb {
    private Connection conn;
    private ConectionFactory conexao = new ConectionFactory();
    private ResultSet rs; 
    private PreparedStatement ps; 
    private String listar = "select * from disciplina";
    
    public ArrayList<Disciplina>findAll(){
     ArrayList<Disciplina> usuarios = new ArrayList<Disciplina>();
     
     try{
         conn = conexao.conectar();
         ps = conn.prepareStatement(listar);
         rs = ps.executeQuery();
         
         while(rs.next()){
             Disciplina u = new Disciplina();
             u.setCod(rs.getInt("COD_DISCIPLINA"));
             u.setNome(rs.getString("NOME"));
             
             usuarios.add(u);             
         }
         
     }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
     return usuarios;
    }
    
    public boolean inserir(Disciplina u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Insert disciplina (NOME) values(?)");
        
        ps.setString(1, u.getNome());
        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
   public boolean atualizar(Disciplina u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("update disciplina set NOME = ? where COD_DISCIPLINA = ? ");
               
        ps.setString(1, u.getNome());
        ps.setInt(2, u.getCod());

        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }  
        
         public boolean excluir (Disciplina a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Delete from disciplina WHERE COD_DISCIPLINA = ?;");
        ps.setInt(1, a.getCod());
        ps.executeUpdate();
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
}
