/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jpbri
 */
public class ConectionFactory {
    private Connection conn;
    
    public Connection conectar() {
        try {
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost/trabalho?user=root");
        } catch(SQLException e) {
            System.out.println("errorororo");
            Logger.getLogger(ConectionFactory.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return conn;
    }
    
    public void desconectar() {
        try {
            this.conn.close();
        } catch(SQLException e) {
            Logger.getLogger(ConectionFactory.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}