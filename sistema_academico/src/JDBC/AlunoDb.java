/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import models.Aluno;

/**
 *
 * @author jpbri
 */
public class AlunoDb {
    private Connection conn;
    private ConectionFactory conexao = new ConectionFactory();
    private ResultSet rs; 
    private PreparedStatement ps;
    private String listar = "select * from alunos";
    
    public ArrayList<Aluno>findAll(){
     ArrayList<Aluno> alunos = new ArrayList<Aluno>();
     
     try{
         conn = conexao.conectar();
         ps = conn.prepareStatement(listar);
         rs = ps.executeQuery();
         
         while(rs.next()){
             Aluno a = new Aluno();
             a.setCod(rs.getInt("COD_ALUNO"));
             a.setNome(rs.getString("NOME"));
             a.setCpf(rs.getString("CPF"));
             a.setidade(rs.getInt("IDADE"));
             
             alunos.add(a);             
         }
         
     }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
     return alunos;
    }
    
      public boolean inserir(Aluno a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Insert into alunos (NOME,CPF,IDADE) values(?,?,?)");
        ps.setString(1, a.getNome());
        ps.setString(2, a.getCpf());
        ps.setInt(3, a.getidade());
        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
  
       public boolean atualizar(Aluno a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("update alunos set NOME = ?,CPF = ?,IDADE = ? where COD_ALUNO = ? ");
        ps.setString(1, a.getNome());
        ps.setString(2, a.getCpf());
        ps.setInt(3, a.getidade());
        ps.setInt(4, a.getCod());
        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }  
        
         public boolean excluir (Aluno a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Delete from alunos WHERE COD_ALUNO = ?;");
        ps.setInt(1, a.getCod());
        ps.executeUpdate();
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }   
}
