/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import models.Curso;

/**
 *
 * @author jpbri
 */
public class CursoDb {
   private Connection conn;
    private ConectionFactory conexao = new ConectionFactory();
    private ResultSet rs; 
    private PreparedStatement ps; 
    private String listar = "select * from cursos";
    
    public ArrayList<Curso>findAll(){
     ArrayList<Curso> usuarios = new ArrayList<Curso>();
     
     try{
         conn = conexao.conectar();
         ps = conn.prepareStatement(listar);
         rs = ps.executeQuery();
         
         while(rs.next()){
             Curso u = new Curso();
             u.setCod(rs.getInt("COD_CURSO"));
             u.setNome(rs.getString("NOME"));
             u.setCod_disciplina(rs.getInt("COD_DISCIPLINA"));
             u.setCod_aluno(rs.getInt("COD_ALUNO"));

             usuarios.add(u);             
         }
         
     }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
     return usuarios;
    }
    
    public boolean inserir(Curso u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Insert into cursos (NOME,COD_DISCIPLINA,COD_ALUNO) values(?,?,?)");
        ps.setString(1, u.getNome());
        ps.setInt(2, u.getCod_disciplina());
        ps.setInt(3, u.getCod_aluno());
      
        
        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
   public boolean atualizar(Curso u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("update cursos set NOME = ?,COD_DISCIPLINA = ?,COD_ALUNO = ? where COD_CURSO = ? ");
        ps.setString(1, u.getNome());
        ps.setInt(2, u.getCod_disciplina());
        ps.setInt(3, u.getCod_aluno());
        ps.setInt(4, u.getCod());
        
        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }  
        
         public boolean excluir (Curso a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Delete from cursos WHERE COD_CURSO = ?;");
        ps.setInt(1, a.getCod());
        ps.executeUpdate();
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
}
