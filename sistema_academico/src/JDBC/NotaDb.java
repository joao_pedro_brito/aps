/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import models.Nota;

/**
 *
 * @author jpbri
 */
public class NotaDb {
    private Connection conn;
    private ConectionFactory conexao = new ConectionFactory();
    private ResultSet rs; 
    private PreparedStatement ps; 
    private String listar = "select * from notas";
    
    public ArrayList<Nota>findAll(){
     ArrayList<Nota> usuarios = new ArrayList<Nota>();
     
     try{
         conn = conexao.conectar();
         ps = conn.prepareStatement(listar);
         rs = ps.executeQuery();
         
         while(rs.next()){
             Nota u = new Nota();
             u.setCod(rs.getInt("COD_CONTROLE"));
             u.setCod_aluno(rs.getInt("COD_ALUNO"));
             u.setCod_disciplina(rs.getInt("COD_DISCIPLINA"));
             u.setNota(rs.getFloat("NOTA"));

             usuarios.add(u);             
         }
         
     }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
     return usuarios;
    }
    
    public boolean inserir(Nota u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Insert into notas (COD_ALUNO,COD_DISCIPLINA,NOTA) values(?,?,?)");
        ps.setInt(1, u.getCod_aluno());
        ps.setInt(2, u.getCod_disciplina());
        ps.setFloat(3, u.getNota());
        
        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
   public boolean atualizar(Nota u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("update notas set COD_ALUNO = ?,COD_DISCIPLINA = ?,NOTA = ? where COD_CONTROLE = ? ");
         ps.setInt(1, u.getCod_aluno());
        ps.setInt(2, u.getCod_disciplina());
        ps.setFloat(3, u.getNota());
        ps.setInt(4, u.getCod());

        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }  
        
         public boolean excluir (Nota a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Delete from notas WHERE COD_CONTROLE = ?;");
        ps.setInt(1, a.getCod());
        ps.executeUpdate();
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
}
