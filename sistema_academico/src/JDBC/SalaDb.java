/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import models.Sala;


/**
 *
 * @author jpbri
 */
public class SalaDb {
    private Connection conn;
    private ConectionFactory conexao = new ConectionFactory();
    private ResultSet rs; 
    private PreparedStatement ps; 
    private String listar = "select * from salas";
    
    public ArrayList<Sala>findAll(){
     ArrayList<Sala> usuarios = new ArrayList<Sala>();
     
     try{
         conn = conexao.conectar();
         ps = conn.prepareStatement(listar);
         rs = ps.executeQuery();
         
         while(rs.next()){
             Sala u = new Sala();
              u.setCod(rs.getInt("COD_CONTROLE"));            
             u.setCod_sala(rs.getInt("COD_SALA"));
             u.setCod_aluno(rs.getInt("COD_ALUNO"));
             
             usuarios.add(u); 
         }
         
     }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
     return usuarios;
    }
    
    public boolean inserir(Sala u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Insert into salas (COD_ALUNO,COD_SALA) values(?,?)");
        
        ps.setInt(1, u.getCod_aluno());
        ps.setInt(2, u.getCod_sala());

        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
   public boolean atualizar(Sala u){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("update salas set COD_ALUNO = ?,COD_SALA = ? where COD_CONTROLE = ? ");
               
        ps.setInt(1, u.getCod_aluno());
        ps.setInt(2, u.getCod_sala());
        ps.setInt(3, u.getCod());


        ps.executeUpdate();
        
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }  
        
         public boolean excluir (Sala a){
        try{
        this.conn = conexao.conectar();
        ps=conn.prepareStatement("Delete from salas WHERE COD_CONTROLE = ?;");
        ps.setInt(1, a.getCod());
        ps.executeUpdate();
        }catch(Exception e ){
         e.printStackTrace();
     }finally{
         conexao.desconectar();
     }
        return true;
    }
    
}
